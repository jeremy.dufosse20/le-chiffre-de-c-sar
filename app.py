# Programme qui crypte et/ou decrypte une chaine de 
# caractere en ajoutant ou en enlevant 8 a sa valeur ASCII.


from tkinter import *

# Prends en entree une chaine de caractère et un int
# et renvoi la chaine de caractère 'crypter' en ajoutant
# 8 a sa valeur ASCII.
def encode(chaine,key):
    nch = ""
    for lettre in chaine:
        nch += chr(ord(lettre)+key)
    return nch

# Sert a envoyer le contenu de la zone de texte dans la 
# fonction 'encode' et à mettre le contenu modifie dans la 
# zone de texte.
def callback_encode():
    chaine_entree = textarea.get("1.0", 'end-1c')
    textarea.delete("1.0",'end-1c')
    chaine_sortie=encode(chaine_entree,8)
    textarea.insert(END,"{}".format(chaine_sortie))


# Prends en entree une chaine de caractère et un int
# et renvoi la chaine de caractère 'decrypter' en enlevant
# 8 a sa valeur ASCII.
def decode(chaine,key):
    nch = ""
    for lettre in chaine:
        nch += chr(ord(lettre) - key)
    return nch

# Sert a envoyer le contenu de la zone de texte dans la 
# fonction 'decode' et à mettre le contenu modifie dans la 
# zone de texte.
def callback_decode():
    chaine_entree = textarea.get("1.0", 'end-1c')
    textarea.delete("1.0",'end-1c')
    chaine_sortie=decode(chaine_entree,8)
    textarea.insert(END,"{}".format(chaine_sortie))



############### Partie IHM  ###############

affichage=Tk()
affichage.title("Le chiffre de César")

lab = Label(affichage,text="Le chiffre de César")
lab.grid(row=0,column=0,padx=10,pady=10)

fenetre = Frame(affichage)

textarea = Text(affichage,width="50",height="5")
textarea.grid(row=1,column=0,padx=20,pady=10,ipady=5)

fenetre.grid(row=2,column=0)

bouton_chiffrement=Button(fenetre,width="10",text="chiffrer")
bouton_dechiffrement=Button(fenetre,width="10",text="dechiffrer")

bouton_chiffrement.pack(side="left")
bouton_dechiffrement.pack(side="right")

affichage.geometry("400x200")

bouton_chiffrement.configure(command=callback_encode)
bouton_dechiffrement.configure(command=callback_decode)

affichage.mainloop()
