# Programme qui test le retour des fonctions "encode" et "decode".

import unittest
import app

class TestFonctionCodage(unittest.TestCase):

    def test_codage(self):
        self.assertEqual(encode("ABCD", 8), "IJKL")
        self.assertEqual(encode("CEQ", 8),"KMY")

    def test_decodage(self):
        self.assertEqual(decode("IJKL", 8), "ABCD")
        self.assertEqual(decode("KMY", 8),"CEQ")
        
